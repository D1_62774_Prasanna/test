const mysql = require('mysql')

const pool = mysql.createPool({
  connectionLimit: 20,
  user: 'root',
  password: 'manager',
  database: 'movie_db',
  port: 3306,
  host: 'localhost',
})

module.exports = {
  pool,
}

