const express = require('express')
const database = require('../database')
const utils = require('../utils')

const router = express.Router()

router.get('/:movie_title',(request,response)=>{
    const {movie_title} = request.params

    const statement = `
    SELECT * FROM 
    movie
    WHERE 
    movie_title = ?
    `
    database.pool.query(statement,[movie_title],(error,result)=>{
        console.log(result);
        if(result.length>0){
            response.send(utils.createResult(error,result))
        }
        else{
            response.send('Movie Not Found!!!')
        }

    })
})

router.post('/add',(request,response)=>{
    const {movie_title, movie_release_date, movie_time, director_name } = request.body

    const statement = `
    INSERT INTO movie
        (movie_title, movie_release_date, movie_time, director_name)
    VALUES
    (? ,?, ?, ?)

    `
    database.pool.query(statement,[movie_title, movie_release_date, movie_time, director_name],(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/update/:movie_id',(request,response)=>{
    const {movie_id} = request.params
    const { movie_release_date, movie_time } = request.body

    const statement = `
    UPDATE movie
    SET
         movie_release_date = ? , movie_time = ?
    WHERE
         movie_id = ? ;

    `
    database.pool.query(statement, [movie_release_date, movie_time,movie_id],(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/delete/:movie_id',(request,response)=>{
    const {movie_id} = request.params

    const statement = `
   DELETE FROM movie
    WHERE
         movie_id = ?

    `
    database.pool.query(statement, [movie_id],(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})
module.exports = router